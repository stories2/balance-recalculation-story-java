package com.epam.engx.stubs;

import com.epam.engx.thirdpartyjar.Service;
import com.epam.engx.thirdpartyjar.Tariff;

import java.util.Collections;
import java.util.List;

public class ServiceStub implements Service {
    @Override
    public List<Tariff> getTariffs() {
        return Collections.<Tariff>singletonList(new UnitBasedTariffStub());
    }

}
