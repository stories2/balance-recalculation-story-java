package com.epam.engx.stubs;

import com.epam.engx.thirdpartyjar.Service;
import com.epam.engx.thirdpartyjar.Tariff;

import java.util.List;

public class MultiTariffServiceStub implements Service {

    private final List<Tariff> tariffs;

    public MultiTariffServiceStub(List<Tariff> tariffs) {
        this.tariffs = tariffs;
    }

    @Override
    public List<Tariff> getTariffs() {
        return tariffs;
    }
}
